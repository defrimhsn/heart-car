package com.heartcar.car.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 9/6/2017.
 */
@Entity
@Table(name = "detail_key")
public class Key {

    @Id
    @Column(name = "id")
    private int id;
    @Column(name="key")
    private String key;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="is_active")
    private boolean active;
    @OneToMany(targetEntity = Value.class,mappedBy = "key")
    @JsonManagedReference("value")
    private List<Value> values;

    public Key(int id, String key, Date createdDate, boolean active) {
        this.id = id;
        this.key = key;
        this.createdDate = createdDate;
        this.active = active;
    }

    public Key() {
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Key{" +
                "key='" + key + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Key key = (Key) o;

        return getKey() != null ? getKey().equals(key.getKey()) : key.getKey() == null;
    }

    @Override
    public int hashCode() {
        return getKey() != null ? getKey().hashCode() : 0;
    }
}
