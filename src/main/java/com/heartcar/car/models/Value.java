package com.heartcar.car.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by defri on 9/6/2017.
 */
@Entity
@Table(name = "detail_value")
public class Value {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @ManyToOne
    @JoinColumn(name = "car_id")
    @JsonBackReference("value")
    private Car car;
    @ManyToOne
    @JoinColumn(name="key_id")
    @JsonBackReference("value")
    private Key key;
    @Column(name="value")
    private String val;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name = "is_active")
    private boolean active;

    public Value(int id, Car car, Key key, Date createdDate, boolean active) {
        this.id = id;
        this.car = car;
        this.key = key;
        this.createdDate = createdDate;
        this.active = active;
    }

    public Value() {

    }

    public Value(int id, Car car, Key key, String val, Date createdDate, boolean active) {
        this.id = id;
        this.car = car;
        this.key = key;
        this.val = val;
        this.createdDate = createdDate;
        this.active = active;
    }

    public int getId() {
        return id;
    }


    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getKeyName(){
        return key.getKey();
    }

    @Override
    public String toString() {
        return getKey().getKey()+" : "+val;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value value = (Value) o;

        if (getCar() != null ? !getCar().equals(value.getCar()) : value.getCar() != null) return false;
        if (getKey() != null ? !getKey().equals(value.getKey()) : value.getKey() != null) return false;
        return getVal() != null ? getVal().equals(value.getVal()) : value.getVal() == null;
    }

    @Override
    public int hashCode() {
        int result = getCar() != null ? getCar().hashCode() : 0;
        result = 31 * result + (getKey() != null ? getKey().hashCode() : 0);
        result = 31 * result + (getVal() != null ? getVal().hashCode() : 0);
        return result;
    }
}
