package com.heartcar.car.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.brand.models.Category;
import com.heartcar.invoice.models.Invoice;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 9/4/2017.
 */
@Entity
@Table(name="car")
public class Car {

    @Id
    @Column(name="id")
    private int id;
    @ManyToOne
    @JoinColumn(name="category_id")
    @JsonBackReference("car")
    private Category category;
    @Column(name="model")
    private String model;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="updated_date")
    private Date updatedDate;
    @Column(name="is_active")
    private boolean isActive;
    @OneToMany(targetEntity = Value.class,mappedBy = "car")
    private List<Value> values;
    @OneToMany(targetEntity = Invoice.class,mappedBy = "car")
    @JsonManagedReference("invoice")
    private List<Invoice> invoices;
    @Column(name = "image_path")
    private String imagePath;



    public Car(int id, Category category, String model, Date createdDate, Date updatedDate, boolean isActive, List<Value> values, List<Invoice> invoices, String imagePath) {
        this.id = id;
        this.category = category;
        this.model = model;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.isActive = isActive;
        this.values = values;
        this.invoices = invoices;
        this.imagePath = imagePath;

    }

    public Car() {
    }

    public int getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getBrandName() {
        return category.getBrand().getName();
    }

    public String getLocation(){
        return category.getBrand().getOffice().getCity()+", "+category.getBrand().getOffice().getCountry();
    }

    @Override
    public String toString() {
        return category.getBrand().getName()+" "+ model+" ("+category.getName()+") "+ "["+category.getBrand().getOffice().getOfficeId()+"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return id == car.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
