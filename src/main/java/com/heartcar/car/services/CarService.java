package com.heartcar.car.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.repositories.UserRepository;
import com.heartcar.administration.services.UserService;
import com.heartcar.brand.models.Category;
import com.heartcar.brand.repositories.CategoryRepository;
import com.heartcar.car.models.Car;
import com.heartcar.car.models.Key;
import com.heartcar.car.models.Value;
import com.heartcar.car.repositories.CarRepository;
import com.heartcar.car.repositories.KeyRepository;
import com.heartcar.car.repositories.ValueRepository;
import org.aspectj.weaver.tools.cache.AbstractIndexedFileCacheBacking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by defri on 9/4/2017.
 */
@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private KeyRepository keyRepository;

    @Autowired
    private ValueRepository valueRepository;

    @Autowired
    private UserService userService;

    public List<Car> getAll(){
        return carRepository.findAll();
    }

    public List<Car> getCarsByActiveParents(boolean bool){
        return carRepository.getCarsByActiveParents(bool);
    }

    public Car getCarById(int id){
        return carRepository.findOne(id);
    }

    public List<Car> findByActive(boolean bool){
        return carRepository.findByisActive(bool);
    }


    public HashMap<String, String> saveCar(Map<String,String> map, String model, int category) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Category requestCategory = new Category();

        requestCategory = categoryRepository.findOne(category);
        if(model==null || model.trim().isEmpty()){
            messageResponse.put("err_msg","Submission failed, car model is empty!");
        } else{
            Car car = new Car();
            car.setModel(model);
            car.setCategory(requestCategory);
            car.setCreatedDate(new Date());
            car.setActive(true);
            car.setImagePath(map.get("image"));
            Car addedCar = carRepository.save(car);
            if(addedCar==null){
                messageResponse.put("err_msg","Submission failed, something went wrong");
            } else {
                messageResponse.put("success_msg","Submission made successfully");
            }
        }

        return messageResponse;
    }

    public HashMap<String,String> disable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Car fromDB = carRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, car not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        Car updated = carRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Car fromDB = carRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        Car updated = carRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String, String> saveDetail(int car, int key,String value) {
        HashMap<String,String> messageResponse = new HashMap<>();
        if(car<1){
            messageResponse.put("err_msg","Submission failed, car not found");
        } else if(key<1){
            messageResponse.put("err_msg","Submission failed, key not found");
        } else if(value==null || value.trim().isEmpty()){
            messageResponse.put("err_msg","Submission failed, value name is empty!");
        } else {
            Car requestCar = carRepository.findOne(car);
            Key requestKey = keyRepository.findOne(key);
            Value valuetoadd = new Value();
            valuetoadd.setCar(requestCar);
            valuetoadd.setKey(requestKey);
            valuetoadd.setVal(value);
            valuetoadd.setActive(true);
            valuetoadd.setCreatedDate(new Date());

            Value addedValue = valueRepository.save(valuetoadd);

            if(addedValue==null){
                messageResponse.put("err_msg","Submission failed, something went wrong");
            } else {
                messageResponse.put("success_msg","Submission made successfully");
            }
        }
        return messageResponse;
    }


    public int countActive() {
        return carRepository.findByisActive(true).size();
    }

    public int countDeactived(){
        return carRepository.findByisActive(false).size();

    }

    public long count() {
        return carRepository.count();
    }

    public List<Car> findActiveCarsByCompany(String name){
        return carRepository.findActiveCarsByCompany(name);
    }

    public List<Car> findActiveCarsbyLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return carRepository.findActiveCarsByCompany(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getName());
    }

    public List<Car> findCarsbyLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return carRepository.findCarsByCompany(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getName());
    }




}
