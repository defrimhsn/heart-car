package com.heartcar.car.services;

import com.heartcar.car.models.Key;
import com.heartcar.car.repositories.KeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by defri on 9/6/2017.
 */
@Service
public class KeyService {

    @Autowired
    private KeyRepository keyRepository;

    public List<Key> getAll(){
        return keyRepository.findAll();
    }

    public Key findbyName(String name){
        return keyRepository.findByKey(name);
    }
}
