package com.heartcar.car.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.car.models.Car;
import com.heartcar.car.models.Value;
import com.heartcar.car.repositories.CarRepository;
import com.heartcar.car.repositories.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by defri on 9/7/2017.
 */
@Service
public class ValueService {

    @Autowired
    private ValueRepository valueRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UserService userService;

    public List<Value> getAll(){
        return valueRepository.findAll();
    }

    public List<Value> getValuesByCar(int id){
        return valueRepository.findByCar(carRepository.findOne(id));
    }

    public HashMap<String,String> disable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Value fromDB = valueRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, car not found");
        }

        fromDB.setActive(false);
        Value updated = valueRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Value fromDB = valueRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }

        fromDB.setActive(true);
        Value updated = valueRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;

    }

    public HashMap<String, String> updateValue(int valId,String value) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Value fromDB = valueRepository.findOne(valId);
        fromDB.setVal(value);
        Value updatedValue = valueRepository.save(fromDB);
        if(updatedValue!=null){
            messageResponse.put("success_msg","Update made successfully");
        } else {
            messageResponse.put("err_msg","Update failed, something went wrong!");
        }
        return messageResponse;
    }

    public List<Value> getValuesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return valueRepository.findValuesByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }
}
