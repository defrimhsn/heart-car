package com.heartcar.car.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.brand.services.CategoryService;
import com.heartcar.car.models.Car;
import com.heartcar.car.models.Value;
import com.heartcar.car.services.CarService;
import com.heartcar.car.services.KeyService;
import com.heartcar.car.services.ValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 9/4/2017.
 */
@Controller
@RequestMapping(value = "/car")
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private KeyService keyService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ValueService valueService;

    @Autowired
    private UserService userService;


    @RequestMapping(method = RequestMethod.GET)
    public String initCarView(Model model){
        model.addAttribute("categories",categoryService.getActiveCategoriesByLoggedUser());
        model.addAttribute("cars",carService.findCarsbyLoggedUser());
        model.addAttribute("keys",keyService.getAll());
        model.addAttribute("values",valueService.getAll());
        model.addAttribute("user",userService.findUserByEmployersUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
        return "car/car";
    }

    @RequestMapping(method = RequestMethod.POST,value = "/{category}/{model}",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveCar(@RequestBody Map<String,String>map, @PathVariable String model,@PathVariable int category){
        return carService.saveCar(map,model,category);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableCar(@PathVariable int id){
        return carService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableCar(@PathVariable int id){
        return carService.enable(id);
    }

    @RequestMapping(value = "/{car}/{key}/{value}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveDetail(@PathVariable int car,@PathVariable int key,@PathVariable String value){
        return carService.saveDetail(car,key,value);
    }

}
