package com.heartcar.car.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.car.models.Car;
import com.heartcar.car.models.Value;
import com.heartcar.car.services.CarService;
import com.heartcar.car.services.KeyService;
import com.heartcar.car.services.ValueService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created by defri on 9/6/2017.
 */
@Controller
@RequestMapping(value = "/key")
public class KeyController {

    @Autowired
    private KeyService keyService;

    @Autowired
    private CarService carService;

    @Autowired
    private ValueService valueService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String initDetails(Model model){
        model.addAttribute("cars",carService.findActiveCarsbyLoggedUser());
        model.addAttribute("values",valueService.getValuesByLoggedUser());
        model.addAttribute("keys",keyService.getAll());
        model.addAttribute("user",userService.getLoggedUser());
        return "car/car-details";
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableValue(@PathVariable int id){
        return valueService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableValue(@PathVariable int id){
        return valueService.enable(id);
    }

    @RequestMapping(value = "/update/{id}/{value}",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> updateValue(@PathVariable int id,@PathVariable String value){
        return valueService.updateValue(id,value);
    }



}
