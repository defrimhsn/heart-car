package com.heartcar.car.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by defri on 9/10/2017.
 */
@Controller
@RequestMapping(value = "/images")
public class ImageController {

    @RequestMapping(method = RequestMethod.GET)
    public String initImageView(Model model){
        return "car/images";
    }
}
