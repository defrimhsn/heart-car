package com.heartcar.car.api;

import com.heartcar.car.models.Car;
import com.heartcar.car.models.Value;
import com.heartcar.car.services.CarService;
import com.heartcar.car.services.ValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by defri on 9/4/2017.
 */
@RestController
@RequestMapping(value = "/api/car")
public class CarRestController {

    @Autowired
    private CarService carService;

    @Autowired
    private ValueService valueService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Car> getCarsByActiveParents(){
        return carService.getCarsByActiveParents(true);
    }

    //example
    @RequestMapping(value = "/{id}/values",method = RequestMethod.GET)
    public List<Value> getValuesByCar(@PathVariable int id){
        return valueService.getValuesByCar(id);
    }

    @RequestMapping(value = "/get/{name}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Car> findActiveCarsByCompany(@PathVariable(name = "name") String name){
        return carService.findActiveCarsByCompany(name);
    }




}
