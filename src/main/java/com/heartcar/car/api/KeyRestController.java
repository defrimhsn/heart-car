package com.heartcar.car.api;

import com.heartcar.car.models.Key;
import com.heartcar.car.services.KeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by defri on 9/6/2017.
 */
@RestController
@RequestMapping(value = "/api/key")
public class KeyRestController {

    @Autowired
    private KeyService keyService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Key> getAll(){
        return keyService.getAll();
    }

    @RequestMapping(value = "/{name}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Key getKeybyName(@PathVariable String name){
        return keyService.findbyName(name);
    }
}
