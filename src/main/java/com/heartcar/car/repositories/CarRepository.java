package com.heartcar.car.repositories;

import com.heartcar.car.models.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by defri on 9/4/2017.
 */
@Repository
public interface CarRepository extends JpaRepository<Car,Integer>{

    @Query("select c from Car c where c.category.active =:bool " +
            "AND c.category.brand.active = :bool and " +
            "c.category.brand.office.isActive =:bool AND" +
            " c.category.brand.office.officeComplex.isActive =:bool")
    List<Car> getCarsByActiveParents(@Param("bool") boolean bool);

    List<Car> findByisActive(boolean bool);

    @Query("select c from Car c where c.isActive = true AND c.category.active =true " +
            "AND c.category.brand.active = true and " +
            "c.category.brand.office.isActive =true AND" +
            " c.category.brand.office.officeComplex.isActive =true AND c.category.brand.office.officeComplex.name= :name")
    List<Car> findActiveCarsByCompany(@Param("name") String name);

    @Query("select c from Car c where c.category.active =true " +
            "AND c.category.brand.active = true and " +
            "c.category.brand.office.isActive =true AND" +
            " c.category.brand.office.officeComplex.isActive =true AND c.category.brand.office.officeComplex.name= :name")
    List<Car> findCarsByCompany(@Param("name") String name);






}
