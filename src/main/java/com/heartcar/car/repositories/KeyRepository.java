package com.heartcar.car.repositories;

import com.heartcar.car.models.Key;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by defri on 9/6/2017.
 */
@Repository
public interface KeyRepository extends JpaRepository<Key,Integer> {
    Key findByKey(String name);
}
