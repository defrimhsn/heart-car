package com.heartcar.car.repositories;

import com.heartcar.car.models.Car;
import com.heartcar.car.models.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by defri on 9/7/2017.
 */
@Repository
public interface ValueRepository extends JpaRepository<Value,Integer> {

    List<Value> findByCar(Car car);

    @Query("SELECT v from Value v where v.car.isActive = true AND " +
            "v.car.category.active = true AND v.car.category.brand.active = true AND " +
            "v.car.category.brand.office.isActive = true AND v.car.category.brand.office.officeComplex.isActive = true AND " +
            "v.car.category.brand.office.officeComplex.id = :companyId")
    List<Value> findValuesByLoggedUser(@Param("companyId") int companyId);
}
