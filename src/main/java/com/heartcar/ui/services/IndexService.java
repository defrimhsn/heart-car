package com.heartcar.ui.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.brand.models.Brand;
import com.heartcar.brand.models.Category;
import com.heartcar.employer.models.Employer;
import com.heartcar.employer.models.Position;
import com.heartcar.invoice.models.Invoice;
import com.heartcar.invoice.services.InvoiceService;
import com.heartcar.office.models.Office;
import com.heartcar.office.services.OfficeComplexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by defri on 9/18/2017.
 */
@Service
public class IndexService {

    @Autowired
    private UserService userService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private OfficeComplexService officeComplexService;

    public int countOffices(){
        return userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getOffices().size();
    }

    public int countEmployers(){
        int employers = 0;
        for(Office office : userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getOffices()){
            for(Position position : office.getPositions()){
                employers+=position.getEmployers().size();
            }
        }
        return employers;
    }

    public int countBrands(){
        int brandCount = 0;
        for(Office office : userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getOffices()){
            brandCount+= office.getBrands().size();
        }
        return brandCount;
    }

    public int countCars(){
        int carCount = 0;
        for(Office office : userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getOffices()){
          for(Brand brand : office.getBrands()){
              for(Category category : brand.getCategories()){
                  carCount+=category.getCars().size();
              }
          }
        }
        return carCount;
    }

    public List<Office> getOffices(){
        return officeComplexService.getActiveOfficesById(userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public Invoice getLastPrintedInvoice(){
        return invoiceService.getLast();
    }

    public List<Invoice> getFiveLastInvoices(){
        return invoiceService.getFiveLastInvoices().subList(0,4);
    }
}
