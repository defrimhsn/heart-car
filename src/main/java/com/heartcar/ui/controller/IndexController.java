package com.heartcar.ui.controller;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.services.OfficeComplexService;
import com.heartcar.office.services.OfficeService;
import com.heartcar.ui.services.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by defri on 8/24/2017.
 */
@Controller
public class IndexController {

    @Autowired
    private UserService userService;

    @Autowired
    private IndexService indexService;


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        model.addAttribute("user",userService.getLoggedUser());
        model.addAttribute("offices",indexService.countOffices());
        model.addAttribute("employers",indexService.countEmployers());
        model.addAttribute("brands",indexService.countBrands());
        model.addAttribute("cars",indexService.countCars());
        model.addAttribute("officeList",indexService.getOffices());
        model.addAttribute("lastInvoice",indexService.getLastPrintedInvoice());
        model.addAttribute("invoices",indexService.getFiveLastInvoices());
        return "index";
    }







}
