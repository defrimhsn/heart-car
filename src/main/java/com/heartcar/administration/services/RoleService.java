package com.heartcar.administration.services;

import com.heartcar.administration.models.Role;
import com.heartcar.administration.repositories.RoleRepository;
import com.heartcar.office.repositories.OfficeComplexRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 9/11/2017.
 */
@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private OfficeComplexRepository officeComplexRepository;

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public   HashMap<String,String> saveRole(Map<String, String> requestParams) {
        HashMap<String,String> messageResponse = new HashMap<>();
        boolean allNotNull = false;
        for(Map.Entry<String,String> entry : requestParams.entrySet()){
            if(entry.getKey()!=null && entry.getValue()!=null){
                allNotNull = true;
            } else{
                allNotNull = false;
            }
        }
        if(allNotNull){
            Role role = new Role();
            role.setName(officeComplexRepository.findOne(Integer.parseInt(requestParams.get("office"))).getName());
            role.setDescription(requestParams.get("description"));
            role.setCreatedDate(new Date());
            role.setActive(true);
            Role addedRole = roleRepository.save(role);
            if(addedRole!=null){
                messageResponse.put("success_msg","Submission made successfully!");
            } else{
                messageResponse.put("err_msg","Submission failed, something went wrong");
            }
        } else{
            messageResponse.put("err_msg","Submission failed, something went wrong during request");
        }

        return messageResponse;
    }
}

