package com.heartcar.administration.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.repositories.RoleRepository;
import com.heartcar.administration.repositories.UserRepository;
import com.heartcar.employer.models.Employer;
import com.heartcar.employer.repositories.EmployerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 9/11/2017.
 */
@Service
public class UserService  {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployerRepository employerRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User findUserByEmployersUsername(String username){
        return userRepository.findByEmployerUsername(username);
    }

    public HashMap<String, String> saveUser(Map<String, String> requestParams) {
        HashMap<String,String> messageResponse = new HashMap<>();
        boolean allNotNull = false;
        for(Map.Entry<String,String> entry : requestParams.entrySet()){
            if(entry.getKey()!=null && entry.getValue()!=null){
                allNotNull = true;
            } else {
                allNotNull = false;
            }
        }
        if(allNotNull){
            Employer requestEmployer = employerRepository.findOne(Integer.parseInt(requestParams.get("employer")));
            if(findUserByEmployersUsername(requestEmployer.getUsername())==null){
                User user = new User();
                user.setEmployer(requestEmployer);
                user.setEmail(requestParams.get("email"));
                user.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
                user.setRole(roleRepository.findOne(Integer.parseInt(requestParams.get("role"))));
                user.setCreatedDate(new Date());
                user.setActive(true);
                User addedUser = userRepository.save(user);
                if(addedUser!=null){
                    messageResponse.put("success_msg","Submission made sucessfilly!");
                } else{
                    messageResponse.put("err_msg","Submission failed, something went wrong");
                }
            } else {
                messageResponse.put("err_msg","Submission failed, user exists already");
            }

        } else{
            messageResponse.put("err_msg","Submission failed, something went wrong during request");
        }
        return messageResponse;
    }

    public HashMap<String,String> disable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        User fromDB = userRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, car not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        User updated = userRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        User fromDB = userRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        User updated = userRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public User getLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = findUserByEmployersUsername(authentication.getName());
        return user;
    }



}
