package com.heartcar.administration.api;

import com.heartcar.administration.models.Role;
import com.heartcar.administration.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

/**
 * Created by defri on 9/13/2017.
 */
@RestController
@RequestMapping(value = "/api/role")
public class RoleRestController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Role> getAll(){
        return roleService.getAll();
    }


}
