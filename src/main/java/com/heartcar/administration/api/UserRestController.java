package com.heartcar.administration.api;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by defri on 9/11/2017.
 */
@RestController
@RequestMapping(value = "/api/user")
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{username}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public User getByUsername(@PathVariable String username){
        return userService.findUserByEmployersUsername(username);
    }
}

