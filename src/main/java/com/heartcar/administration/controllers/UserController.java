package com.heartcar.administration.controllers;

import com.heartcar.administration.services.RoleService;
import com.heartcar.administration.services.UserService;
import com.heartcar.employer.services.EmployerService;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.services.OfficeComplexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by defri on 9/11/2017.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private EmployerService employerService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private OfficeComplexService officeComplexService;

    @RequestMapping(method = RequestMethod.GET)
    public String initUserView(Model model){
        model.addAttribute("users",userService.getAll());
        model.addAttribute("roles",roleService.getAll());
        model.addAttribute("employers",employerService.getAll());
        model.addAttribute("companies",officeComplexService.getByActive(true));
        model.addAttribute("user",userService.findUserByEmployersUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
        return "user/user";
    }

    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveUser(@RequestBody Map<String,String> requestParams){
        return userService.saveUser(requestParams);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableUser(@PathVariable int id){
        return userService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableUser(@PathVariable int id){
        return userService.enable(id);
    }





}
