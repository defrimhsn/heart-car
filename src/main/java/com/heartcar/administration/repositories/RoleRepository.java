package com.heartcar.administration.repositories;

import com.heartcar.administration.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by defri on 9/11/2017.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
}
