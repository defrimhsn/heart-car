package com.heartcar.administration.repositories;

import com.heartcar.administration.models.User;
import com.heartcar.employer.models.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by defri on 9/11/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User,Integer>{

    @Query("select u from User u where u.employer.username =:username")
    User findByEmployerUsername(@Param("username") String username);

    @Query("SELECT u from User u where u.employer.username =:username AND u.password =:password")
    User findbyUsernameAndPassword(@Param("username") String username,@Param("password") String password);



}
