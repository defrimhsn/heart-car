package com.heartcar.administration.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.employer.models.Employer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 9/11/2017.
 */
@Entity
@Table(name ="user")
public class User {

    @Id
    @Column(name = "id")
    private int id;
    @Column(name="password")
    private String password;
    @Column(name="email")
    private String email;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="updated_date")
    private Date updatedDate;
    @Column(name="is_active")
    private boolean active;

    @ManyToOne
    @JoinColumn(name="employer_id")
    @JsonBackReference("user")
    private Employer employer;

    @ManyToOne
    @JoinColumn(name="role_id")
    @JsonBackReference("role")
    private Role role;

    public User(int id, String password, String email, Date createdDate, Date updatedDate, boolean active, Employer employer, Role role) {
        this.id = id;
        this.password = password;
        this.email = email;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.active = active;
        this.employer = employer;
        this.role = role;
    }

    public User() {
    }

    public String getUsername(){
        return employer.getUsername();
    }

    public void setUsername(String username){
        this.employer.setUsername(username);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                ", active=" + active +
                ", employer=" + employer +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return getEmployer() != null ? getEmployer().equals(user.getEmployer()) : user.getEmployer() == null;
    }

    @Override
    public int hashCode() {
        return getEmployer() != null ? getEmployer().hashCode() : 0;
    }
}
