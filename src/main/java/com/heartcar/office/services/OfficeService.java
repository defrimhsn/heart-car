package com.heartcar.office.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.office.models.Office;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.repositories.OfficeComplexRepository;
import com.heartcar.office.repositories.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Service
public class OfficeService {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private OfficeComplexRepository officeComplexRepository;

    public int count(){
        return Integer.parseInt(officeRepository.count()+"");
    }

    public List<Office> getByActive(boolean bool){
        return officeRepository.findByisActive(bool);
    }

    public OfficeComplex getComplex(int id){
        return officeRepository.findOne(id).getOfficeComplex();
    }

    public Office getById(int id){
        return officeRepository.findOne(id);
    }

    public List<Office> getAll() {
        return officeRepository.findAll();
    }

    public List<Office> getOfficesByActiveComplex(){
        return officeRepository.findbyActiveComplex(true);
    }

    public List<Office> getOfficesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return officeRepository.findActiveOfficesByComplex(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public List<Office> getActiveOfficesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return officeRepository.findActiveOfficesByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public HashMap<String,String> saveOffice(Map<String,String> office, int complex) {
        OfficeComplex requestComplex = new OfficeComplex();
        HashMap<String,String> messageResponse = new HashMap<>();
        requestComplex = officeComplexRepository.findOne(complex);
        if(requestComplex==null){
            messageResponse.put("err_msg","Submission failed, company not found");
        } else {
            if(office.get("officeId")==null||office.get("officeId").trim().isEmpty()){
                messageResponse.put("err_msg","Submission failed, office id is empty");
            } else if(office.get("city")==null || office.get("city").trim().isEmpty()){
                messageResponse.put("err_msg","Submission failed, city name is empty");
            } else if(office.get("country")==null || office.get("country").trim().isEmpty()){
                messageResponse.put("err_msg","Submission failed, city name is empty");
            } else if(office.get("phoneNumber")==null || office.get("phoneNumber").trim().isEmpty()){
                messageResponse.put("err_msg","Submission failed, phoneNumber name is empty");
            } else {
                if(officeRepository.findOfficesByOfficeId(office.get("officeId"),userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getId()).size()>0){
                    messageResponse.put("err_msg","Submission failed, office exists already");
                } else {
                    Office officeToAdd = new Office();
                    officeToAdd.setOfficeComplex(requestComplex);
                    officeToAdd.setCreatedDate(new Date());
                    officeToAdd.setActive(true);
                    officeToAdd.setOfficeId(office.get("officeId"));
                    officeToAdd.setStreet(office.get("street"));
                    officeToAdd.setZipCode(office.get("zipCode"));
                    officeToAdd.setPhoneNumber(office.get("phoneNumber"));
                    officeToAdd.setCity(office.get("city"));
                    officeToAdd.setCountry(office.get("country"));

                    Office createdOffice = officeRepository.save(officeToAdd);

                    if(createdOffice!=null){
                        messageResponse.put("success_msg","Submission made successfully");
                    } else {
                        messageResponse.put("err_msg","Submission failed, something is wrong..");
                    }
                }
            }
        }
        return messageResponse;
    }

    public HashMap<String,String> disable(int id){
        HashMap<String,String> messageResponse = new HashMap<>();
        Office fromDB = officeRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, office not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        Office updated = officeRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id){
        HashMap<String,String> messageResponse = new HashMap<>();
        Office fromDB = officeRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        Office updated = officeRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }


    public HashMap<String,String> updateOffice(Map<String,String> map) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Office fromDB = officeRepository.findOne(Integer.parseInt(map.get("id")));
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, office not found");
        } else{
            if(map.get("city")==null || map.get("city").trim().isEmpty()){
                messageResponse.put("err_msg","Update failed, city name is empty");
            } else if(map.get("country")==null || map.get("country").trim().isEmpty()){
                messageResponse.put("err_msg","Update failed, country name is empty");
            } else if(map.get("phoneNumber")==null || map.get("phoneNumber").trim().isEmpty()){
                messageResponse.put("err_msg","Update failed, phone number is empty");
            } else {
                fromDB.setStreet(map.get("street"));
                fromDB.setCity(map.get("city"));
                fromDB.setCountry(map.get("country"));
                fromDB.setZipCode(map.get("zipCode"));
                fromDB.setPhoneNumber(map.get("phoneNumber"));
                fromDB.setUpdatedDate(new Date());
                Office updatedOffice = officeRepository.save(fromDB);

                if(updatedOffice!=null){
                    messageResponse.put("success_msg","Update made successfully");
                } else {
                    messageResponse.put("err_msg","Update failed, something went wrong");
                }
        }
    }
        return messageResponse;
    }

}
