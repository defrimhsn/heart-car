package com.heartcar.office.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.office.models.Office;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.repositories.OfficeComplexRepository;
import com.heartcar.office.repositories.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Service
public class OfficeComplexService {

    @Autowired
    private OfficeComplexRepository officeComplexRepository;

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private UserService userService;


    //
    public List<OfficeComplex> getAll(){
        return officeComplexRepository.findAll();
    }

    public List<OfficeComplex> getByActive(boolean bool){
        return officeComplexRepository.findByisActive(bool);
    }

    public List<Office> getOfficesByComplex(int id){
        OfficeComplex fromDB = officeComplexRepository.findOne(id);
        return fromDB.getOffices();
    }

    public List<OfficeComplex> getComplexesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return officeComplexRepository.findByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public HashMap<String,String> saveOfficeComplex(Map<String,String> requestParams){
        HashMap<String,String> messageResponse = new HashMap<>();
       // HashMap<String,String> detailsResponse = new HashMap<>();
        if(requestParams.get("name") ==null){
            messageResponse.put("err_msg","Submission failed, name is empty");
        } else if(requestParams.get("slogan") ==null){
            messageResponse.put("err_msg","Submission failed, slogan is empty!");
        } else if(requestParams.get("startDate")==null){
            messageResponse.put("err_msg","Submission failed, start date is empty");
        } else {
            OfficeComplex officeComplex = new OfficeComplex();
            officeComplex.setName(requestParams.get("name"));
            officeComplex.setSlogan(requestParams.get("slogan"));
            officeComplex.setAddedDate(new Date());
            officeComplex.setActive(true);
            DateFormat format = new SimpleDateFormat("dd/MM/YYYY");
            try {
                officeComplex.setStartDate(format.parse(requestParams.get("startDate")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            OfficeComplex addedComplex = officeComplexRepository.save(officeComplex);

            if(addedComplex!=null){
                messageResponse.put("success_msg","Submission made successfully");
            } else{
                messageResponse.put("err_msg","Submission failed, something went wrong");
            }
        }
        return messageResponse;
    }

    public int countByActive(boolean bool){
        return officeComplexRepository.findByisActive(bool).size();
    }

    public int count(){
        return Integer.parseInt(officeComplexRepository.count()+"");
    }

    public HashMap<String,String> disable(int id){
        HashMap<String,String> messageResponse = new HashMap<>();
        OfficeComplex fromDB = officeComplexRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        OfficeComplex updated = officeComplexRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id){
        HashMap<String,String> messageResponse = new HashMap<>();
        OfficeComplex fromDB = officeComplexRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        OfficeComplex updated = officeComplexRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }


    public HashMap<String,String> updateComplex(Map<String,String> requestMap) {
        HashMap<String,String> messageResponse = new HashMap<>();
        if(requestMap.get("id") == null){
            messageResponse.put("err_msg","Update failed, id is not null");
        } else  if(requestMap.get("name") ==null){
            messageResponse.put("err_msg","Update failed, name is empty");
        } else if(requestMap.get("slogan") ==null){
            messageResponse.put("err_msg","Update failed, slogan is empty!");
        } else{
            OfficeComplex officeComplex = officeComplexRepository.findOne(Integer.parseInt(requestMap.get("id")));
            officeComplex.setSlogan(requestMap.get("slogan"));
            officeComplex.setDescription(requestMap.get("description"));
            officeComplex.setUpdatedDate(new Date());

            OfficeComplex updatedComplex = officeComplexRepository.save(officeComplex);

            if(updatedComplex!=null){
                messageResponse.put("success_msg","Update made successfully");
            } else{
                messageResponse.put("err_msg","Update failed, something went wrong");
            }
        }
        return messageResponse;
    }

    public List<Office> getActiveOfficesById(int id) {
        return officeRepository.findActiveOfficesByComplex(id);
    }

    public OfficeComplex getOne(int id) {
        return officeComplexRepository.findOne(id);
    }
}
