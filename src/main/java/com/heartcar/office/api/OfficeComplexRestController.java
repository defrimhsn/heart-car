package com.heartcar.office.api;

import com.heartcar.office.models.Office;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.services.OfficeComplexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.print.attribute.standard.Media;
import java.util.List;

/**
 * Created by defri on 8/31/2017.
 */
@RestController
@RequestMapping(value="/api/office-complex")
public class OfficeComplexRestController {

    @Autowired
    private OfficeComplexService officeComplexService;

    @RequestMapping(method = RequestMethod.GET)
    public List<OfficeComplex> getAll(){
        return officeComplexService.getAll();
    }

    @RequestMapping(value = "/{id}/offices",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Office> getActiveOffices(@PathVariable int id){
        return officeComplexService.getActiveOfficesById(id);
    }

    @RequestMapping(value = "/get/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public OfficeComplex getComplex(@PathVariable int id){
        return  officeComplexService.getOne(id);
    }
}
