package com.heartcar.office.api;

import com.heartcar.office.models.Office;
import com.heartcar.office.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by defri on 9/3/2017.
 */
@RestController
@RequestMapping(value = "/api/office")
public class OfficeRestController {

    @Autowired
    private OfficeService officeService;

    @RequestMapping(value = "/activeComplex",produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
    public List<Office> getOfficesByActiveComplex(){
        return officeService.getOfficesByActiveComplex();
    }
}
