package com.heartcar.office.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.brand.models.Brand;
import com.heartcar.employer.models.Position;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Entity
public class Office implements Serializable {

    @Id
    @Column(name="id")
    private int id;
    @Column(name="office_id")
    private String officeId;
    @Column(name="street")
    private String street;
    @Column(name="zip_code")
    private String zipCode;
    @Column(name="city")
    private String city;
    @Column(name="country")
    private String country;
    @Column(name="phone_no")
    private String phoneNumber;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="updated_date")
    private Date updatedDate;
    @Column(name="is_active")
    private boolean isActive;
    @ManyToOne
    @JoinColumn(name = "complex_id")
    @JsonBackReference("office")
    private OfficeComplex officeComplex;

    @OneToMany(targetEntity = Brand.class,mappedBy = "office")
    @JsonManagedReference(value="brand")
    private List<Brand> brands = new ArrayList<>();

    @OneToMany(targetEntity = Position.class,mappedBy = "office")
    @JsonManagedReference(value = "position")
    private List<Position> positions = new ArrayList<>();


    public Office(int id, String officeId, String street, String zipCode, String city, String country, String phoneNumber, Date createdDate, Date updatedDate, boolean isActive, OfficeComplex officeComplex, List<Brand> brands, List<Position> positions) {
        this.id = id;
        this.officeId = officeId;
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.isActive = isActive;
        this.officeComplex = officeComplex;
        this.brands = brands;
        this.positions = positions;
    }

    public Office() {

    }

    public List<Brand> getBrands() {
        return brands;
    }

    public int getId() {
        return id;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public OfficeComplex getOfficeComplex() {
        return officeComplex;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public void setOfficeComplex(OfficeComplex officeComplex) {
        this.officeComplex = officeComplex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Office office = (Office) o;

        if (getId() != office.getId()) return false;
        return getOfficeId() == office.getOfficeId();
    }


    @Override
    public String toString() {
        return "Office{" +
                "id=" + id +
                ", officeId='" + officeId + '\'' +
                ", street='" + street + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                ", isActive=" + isActive +
                ", officeComplex=" + officeComplex +
                '}';
    }
}
