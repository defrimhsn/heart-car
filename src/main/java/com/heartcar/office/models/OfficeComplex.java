package com.heartcar.office.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Entity
public class OfficeComplex implements Serializable {

    @Id
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;
    @Column(name="slogan")
    private String slogan;
    @Column(name="description")
    private String description;
    @Column(name="start_date")
    @JsonFormat(pattern="dd/MM/yyyy", timezone="America/Phoenix")
    private Date startDate;
    @Column(name="created_date")
    private Date addedDate;
    @Column(name="updated_date")
    private Date updatedDate;
    @Column(name="is_active")
    private boolean isActive;

    @OneToMany(targetEntity = Office.class,mappedBy = "officeComplex")
    @JsonManagedReference(value="office")
    private List<Office> offices = new ArrayList<>();

    public OfficeComplex(){

    }

    public OfficeComplex(int id, String name, String slogan, String description, Date startDate, Date addedDate, boolean isActive) {
        this.id = id;
        this.name = name;
        this.slogan = slogan;
        this.description = description;
        this.startDate = startDate;
        this.addedDate = addedDate;
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Office> getOffices() {
        return offices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OfficeComplex that = (OfficeComplex) o;

        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @JsonGetter
    @Override
    public String toString() {
        return "OfficeComplex{" +
                "name='" + name + '\'' +
                ", slogan='" + slogan + '\'' +
                '}';
    }

}
