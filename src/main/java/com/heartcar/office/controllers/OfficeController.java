package com.heartcar.office.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.office.models.Office;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.services.OfficeComplexService;
import com.heartcar.office.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.net.Authenticator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Controller
@RequestMapping(value = "/office")
public class OfficeController {

    @Autowired
    private OfficeService officeService;

    @Autowired
    private OfficeComplexService officeComplexService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String initOffice(Model model){
        model.addAttribute("offices",officeService.getOfficesByLoggedUser());
        model.addAttribute("complexes",officeComplexService.getComplexesByLoggedUser());
        model.addAttribute("user",userService.getLoggedUser());
        return "office/office";
    }

    @RequestMapping(value="/count",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public int countOffices(){
        return officeService.count();
    }

    @RequestMapping(value="/{id}/getComplex",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public OfficeComplex getComplexByOffice(@PathVariable int id){
        return officeService.getComplex(id);
    }

    @RequestMapping(value="{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Office getById(@PathVariable int id){
        return officeService.getById(id);
    }

    @RequestMapping(value="/{complex}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveOffice(@RequestBody Map<String,String> office, @PathVariable int complex){
        return officeService.saveOffice(office,complex);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableOffice(@PathVariable int id){
        return officeService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableOffice(@PathVariable int id){
        return officeService.enable(id);
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> update(@RequestBody  Map<String,String> map){
        return officeService.updateOffice(map);
    }




}
