package com.heartcar.office.controllers;

import com.heartcar.office.models.Office;
import com.heartcar.office.models.OfficeComplex;
import com.heartcar.office.services.OfficeComplexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.servlet.http.HttpServletResponse;
import java.net.Authenticator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Controller
@RequestMapping(value = "/office-complex")
public class OfficeComplexController {

    @Autowired
    private OfficeComplexService officeComplexService;

    @ModelAttribute("officeComplex")
    public OfficeComplex getOfficeComplexObject(){
        return new OfficeComplex();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String initOfficeComplex(Model model){
        model.addAttribute("complexes",officeComplexService.getAll());
        return "office/office-complex";
    }

    @RequestMapping(value="/active/{bool}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OfficeComplex> getByIsActive(@PathVariable boolean bool){
        return officeComplexService.getByActive(bool);
    }

    @RequestMapping(value="/offices/{id}",method = RequestMethod.GET)
    public List<Office> getOfficesByComplex(@PathVariable int id){
        return officeComplexService.getOfficesByComplex(id);
    }

    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveOfficeComplex(@RequestBody Map<String,String> requestParams){
        return officeComplexService.saveOfficeComplex(requestParams);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableComplex(@PathVariable int id){
        return officeComplexService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableComplex(@PathVariable int id){
        return officeComplexService.enable(id);
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> updateComplex(@RequestBody Map<String,String> map){
        return officeComplexService.updateComplex(map);
    }


}
