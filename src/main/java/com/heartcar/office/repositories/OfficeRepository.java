package com.heartcar.office.repositories;

import com.heartcar.office.models.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Repository
public interface OfficeRepository extends JpaRepository<Office,Integer> {
    List<Office> findByisActive(boolean bool);

    @Query("SELECT o from Office o where o.officeComplex.isActive= :bool")
    List<Office> findbyActiveComplex(@Param("bool")boolean bool);

    @Query("SELECT o from Office o where o.officeComplex.id =:id AND o.officeComplex.isActive=true")
    List<Office> findActiveOfficesByComplex(@Param("id") int id);

    @Query("SELECT o from Office o where o.isActive=true AND o.officeComplex.id =:id AND o.officeComplex.isActive=true")
    List<Office> findActiveOfficesByLoggedUser(@Param("id") int id);

    @Query("SELECT o from Office o where o.officeId = :officeId AND o.officeComplex.isActive = true AND o.officeComplex.id = :complexId")
    List<Office> findOfficesByOfficeId(@Param("officeId") String officeId, @Param("complexId") int complexId);

}
