package com.heartcar.office.repositories;

import com.heartcar.office.models.OfficeComplex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Defrim Hasani on 8/17/2017.
 */
@Repository
public interface OfficeComplexRepository extends JpaRepository<OfficeComplex,Integer> {
    List<OfficeComplex> findByisActive(boolean bool);

    @Query("SELECT o from OfficeComplex o where o.id = :id and o.isActive = true")
    List<OfficeComplex> findByLoggedUser(@Param("id") int id);


}
