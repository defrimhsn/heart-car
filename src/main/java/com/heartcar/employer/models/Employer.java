package com.heartcar.employer.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.administration.models.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 9/11/2017.
 */
@Entity
@Table(name = "employer")
public class Employer implements Serializable{

    @Id
    @Column(name="id")
    private int id;
    @Column(name="personal_no")
    private String personalNumber;
    @ManyToOne
    @JoinColumn(name = "pos_id")
    @JsonBackReference("employer")
    private Position position;
    @Column(name="position_added_date")
    private Date positionAddedDate;
    @Column(name="position_updated_date")
    private Date positionUpdatedDate;
    @Column(name="name")
    private String name;
    @Column(name="surname")
    private String surname;
    @Column(name="username")
    private String username;
    @Column(name="dob")
    @JsonFormat(pattern="dd/MM/yyyy", timezone="America/Phoenix")
    private Date dob;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="updated_date")
    private Date updatedDate;
    @Column(name="is_active")
    private boolean active;

    @OneToMany(targetEntity = User.class,mappedBy = "employer")
    @JsonManagedReference("user")
    private List<User> users = new ArrayList<>();

    public Employer() {
    }

    public Employer(int id, String personalNumber, Position position, Date positionAddedDate, Date positionUpdatedDate, String name, String surname, String username, Date dob, Date createdDate, Date updatedDate, boolean active, List<User> users) {
        this.id = id;
        this.personalNumber = personalNumber;
        this.position = position;
        this.positionAddedDate = positionAddedDate;
        this.positionUpdatedDate = positionUpdatedDate;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.dob = dob;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.active = active;
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Date getPositionAddedDate() {
        return positionAddedDate;
    }

    public void setPositionAddedDate(Date positionAddedDate) {
        this.positionAddedDate = positionAddedDate;
    }

    public Date getPositionUpdatedDate() {
        return positionUpdatedDate;
    }

    public void setPositionUpdatedDate(Date positionUpdatedDate) {
        this.positionUpdatedDate = positionUpdatedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getCity(){
        return position.getOffice().getCity();
    }

    public String getCountry(){
        return position.getOffice().getCountry();
    }

    @Override
    public String toString() {
       return personalNumber+" : "+name+" "+surname+" ["+username+"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employer employer = (Employer) o;

        return getPersonalNumber() != null ? getPersonalNumber().equals(employer.getPersonalNumber()) : employer.getPersonalNumber() == null;
    }

    @Override
    public int hashCode() {
        return getPersonalNumber() != null ? getPersonalNumber().hashCode() : 0;
    }
}
