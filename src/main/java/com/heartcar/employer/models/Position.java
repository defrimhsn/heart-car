package com.heartcar.employer.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.office.models.Office;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 9/11/2017.
 */
@Entity
@Table(name = "emp_position")
public class Position implements Serializable{

    @Id
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "office_id")
    @JsonBackReference("position")
    private Office office;
    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "updated_date")
    private Date updatedDate;
    @Column(name = "is_active")
    private boolean active;
    @OneToMany(targetEntity = Employer.class,mappedBy = "position")
    @JsonManagedReference("employer")
    private List<Employer> employers = new ArrayList<>();

    public Position() {
    }

    public Position(int id, String title, String description, Office office, Date createdDate, Date updatedDate, boolean active, List<Employer> employers) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.office = office;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.active = active;
        this.employers = employers;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Employer> getEmployers() {
        return employers;
    }

    public void setEmployers(List<Employer> employers) {
        this.employers = employers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (getTitle() != null ? !getTitle().equals(position.getTitle()) : position.getTitle() != null) return false;
        return getOffice() != null ? getOffice().equals(position.getOffice()) : position.getOffice() == null;
    }

    @Override
    public int hashCode() {
        int result = getTitle() != null ? getTitle().hashCode() : 0;
        result = 31 * result + (getOffice() != null ? getOffice().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Position{" +
                "title='" + title + '\'' +
                ", office=" + office +
                '}';
    }
}
