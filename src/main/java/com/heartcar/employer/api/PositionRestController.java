package com.heartcar.employer.api;

import com.heartcar.employer.models.Position;
import com.heartcar.employer.repositories.PositionRepository;
import com.heartcar.employer.services.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by defri on 9/11/2017.
 */
@RestController
@RequestMapping(value = "/api/office/positions")
public class PositionRestController {

    @Autowired
    private PositionService positionService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Position> getAll(){
        return positionService.getAll();
    }
}
