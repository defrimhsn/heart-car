package com.heartcar.employer.controllers;

import com.heartcar.employer.models.Employer;
import com.heartcar.employer.models.Position;
import com.heartcar.employer.services.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created by defri on 9/11/2017.
 */
@Controller
@RequestMapping(value = "/position")
public class PositionController {

    @Autowired
    private PositionService positionService;

    @RequestMapping(value = "/{office}/{title}/{description}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> savePosition(@PathVariable int office,@PathVariable String title,@PathVariable String description){
        return positionService.savePosition(office,title,description);
    }


}
