package com.heartcar.employer.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.employer.models.Employer;
import com.heartcar.employer.models.Position;
import com.heartcar.employer.services.EmployerService;
import com.heartcar.employer.services.PositionService;
import com.heartcar.office.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by defri on 9/11/2017.
 */
@Controller
@RequestMapping(value = "/employer")
public class EmployerController {

    @Autowired
    private EmployerService employerService;

    @Autowired
    private OfficeService officeService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private UserService userService;


    @RequestMapping(method = RequestMethod.GET)
    public String initEmployerView(Model model){
        model.addAttribute("employers",employerService.getAll());
        model.addAttribute("offices",officeService.getByActive(true));
        model.addAttribute("positions",positionService.getAll());
        model.addAttribute("user",userService.findUserByEmployersUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
        return "employer/employer";
    }

    @RequestMapping(method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String, String> saveEmployer(@RequestBody Map<String,String> map) throws ParseException {
        return employerService.saveEmployer(map);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableEmployer(@PathVariable int id){
        return employerService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableEmployer(@PathVariable int id){
        return employerService.enable(id);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Employer getById(@PathVariable  int id){
        return employerService.getOne(id);
    }




}
