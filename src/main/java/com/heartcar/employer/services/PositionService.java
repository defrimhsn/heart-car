package com.heartcar.employer.services;

import com.heartcar.employer.models.Position;
import com.heartcar.employer.repositories.PositionRepository;
import com.heartcar.office.models.Office;
import com.heartcar.office.services.OfficeService;
import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by defri on 9/11/2017.
 */
@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private OfficeService officeService;

    public List<Position> getAll(){
        return positionRepository.findAll();
    }

    public HashMap<String, String> savePosition(int office,String title,String description) {
        Office requestOffice = officeService.getById(office);
        HashMap<String, String> responseMessage = new HashMap<>();
        if (requestOffice == null) {
            responseMessage.put("err_msg", "Submission failed, office not found");
        } else {
            Position position = new Position();
            position.setTitle(title);
            position.setDescription(description);
            position.setActive(true);
            position.setCreatedDate(new Date());
            position.setOffice(requestOffice);

            Position addedPos = positionRepository.save(position);

            if(addedPos==null){
                responseMessage.put("err_msg", "Submission failed, something went wrong");
            } else {
                responseMessage.put("success_msg","Submission made successfully!");
            }
        }
        return responseMessage;
    }
}
