package com.heartcar.employer.services;

import com.heartcar.car.models.Car;
import com.heartcar.employer.models.Employer;
import com.heartcar.employer.models.Position;
import com.heartcar.employer.repositories.EmployerRepository;
import com.heartcar.employer.repositories.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 9/11/2017.
 */
@Service
public class EmployerService {

    @Autowired
    private EmployerRepository employerRepository;

    @Autowired
    private PositionRepository positionRepository;

    public List<Employer> getAll(){
        return employerRepository.findAll();
    }

    public HashMap<String,String> saveEmployer(Map<String, String> map) throws ParseException {
        HashMap<String,String> messageResponse = new HashMap<>();
        boolean allNotNull = false;
        for(Map.Entry<String,String> entry : map.entrySet()){
            if(entry.getKey()!= null && entry.getValue()!=null){
                allNotNull = true;
            } else{
                allNotNull = false;
            }
        }
        if(allNotNull){
            Position requestPosition = positionRepository.findOne(Integer.parseInt(map.get("position")));
            Employer employer = new Employer();
            employer.setPersonalNumber(map.get("personalNumber"));
            employer.setName(map.get("name"));
            employer.setSurname(map.get("surname"));
            employer.setUsername(map.get("username"));
            employer.setPosition(requestPosition);
            employer.setPositionAddedDate(new Date());
            DateFormat format = new SimpleDateFormat("dd/MM/YYYY");
            String requestDate = map.get("dob");
            Date dateOfBirth = format.parse(requestDate);
            employer.setDob(dateOfBirth);
            employer.setCreatedDate(new Date());
            employer.setActive(true);

            Employer addedEmployer = employerRepository.save(employer);
            if(addedEmployer==null){
                messageResponse.put("err_msg","Submission failed, something went wrong");
            } else{
                messageResponse.put("success_msg","Submission made successfully!");
            }
        } else{
            messageResponse.put("err_msg","Submission failed, something went wrong during the request");
        }

        return messageResponse;
    }

    public HashMap<String,String> disable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Employer fromDB = employerRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, car not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        Employer updated = employerRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Employer fromDB = employerRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        Employer updated = employerRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public Employer getOne(int id) {
        return employerRepository.findOne(id);
    }
}
