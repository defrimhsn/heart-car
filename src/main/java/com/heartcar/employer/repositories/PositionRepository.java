package com.heartcar.employer.repositories;

import com.heartcar.employer.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by defri on 9/11/2017.
 */
@Repository
public interface PositionRepository extends JpaRepository<Position,Integer>{
}
