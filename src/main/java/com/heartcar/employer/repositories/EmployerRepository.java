package com.heartcar.employer.repositories;

import com.heartcar.employer.models.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by defri on 9/11/2017.
 */
@Repository
public interface EmployerRepository extends JpaRepository<Employer,Integer>{
}
