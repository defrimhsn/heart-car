package com.heartcar.brand.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.brand.models.Brand;
import com.heartcar.brand.services.BrandService;
import com.heartcar.office.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 8/31/2017.
 */
@Controller
@RequestMapping(value = "/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    @Autowired
    private OfficeService officeService;

    @Autowired
    private UserService userService;


    public List<Brand> getAll(){
        return brandService.getAll();

    }
    @RequestMapping(method = RequestMethod.GET)
    public String initBrand(Model model){
        model.addAttribute("brands",brandService.getBrandsByLoggedUser());
        model.addAttribute("offices",officeService.getActiveOfficesByLoggedUser());
        model.addAttribute("user",userService.getLoggedUser());
        return "category/brand";
    }

    @RequestMapping(value = "/{office}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveBrand(@RequestBody Map<String,String> map, @PathVariable int office) {
        return brandService.saveBrand(map,office);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableComplex(@PathVariable int id){
        return brandService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableComplex(@PathVariable int id){
        return brandService.enable(id);
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> updateBrand(@RequestBody Map<String,String> map){
        return brandService.updateBrand(map);
    }




}
