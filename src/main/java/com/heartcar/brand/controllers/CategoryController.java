package com.heartcar.brand.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.brand.models.Brand;
import com.heartcar.brand.models.Category;
import com.heartcar.brand.services.BrandService;
import com.heartcar.brand.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by defri on 8/31/2017.
 */
@Controller
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String initCategory(Model model){
        model.addAttribute("brands",brandService.getActiveBrandsByLoggedUser());
        model.addAttribute("categories",categoryService.getCategoriesByLoggedUser());
        model.addAttribute("user",userService.getLoggedUser());
        return "category/category";
    }

    @RequestMapping(value = "/{brand}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveBrand(@RequestBody Map<String,String> map, @PathVariable int brand) {
        return categoryService.saveCategory(map,brand);
    }

    @RequestMapping(value = "/disable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> disableComplex(@PathVariable int id){
        return categoryService.disable(id);
    }

    @RequestMapping(value = "/enable/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> enableComplex(@PathVariable int id){
        return categoryService.enable(id);
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> updateBrand(@RequestBody Map<String,String> map){
        return categoryService.updateBrand(map);
    }
}
