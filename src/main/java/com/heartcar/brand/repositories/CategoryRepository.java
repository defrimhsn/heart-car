package com.heartcar.brand.repositories;

import com.heartcar.brand.models.Brand;
import com.heartcar.brand.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by defri on 8/31/2017.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

    @Query("select c from  Category c where c.brand.active = :bool and c.brand.office.isActive =:bool AND c.brand.office.officeComplex.isActive =:bool")
    List<Category> findCategoriesByActiveParents(@Param("bool") boolean bool);

    @Query("SELECT c from Category c where c.brand.active = true and c.brand.office.isActive = true AND c.brand.office.officeComplex.isActive = true AND c.brand.office.officeComplex.id = :id")
    List<Category> findCategoriesByLoggedUser(@Param("id") int company);

    @Query("SELECT c from Category c where c.active=true AND c.brand.active = true and c.brand.office.isActive = true AND c.brand.office.officeComplex.isActive = true AND c.brand.office.officeComplex.id = :id")
    List<Category> findActiveCategoriesByLoggedUser(@Param("id") int company);

    @Query("SELECT c from Category c where c.name= :name and c.brand.id = :brandId")
    Category findCategoryByName(@Param("name") String name, @Param("brandId") int brandId);

}
