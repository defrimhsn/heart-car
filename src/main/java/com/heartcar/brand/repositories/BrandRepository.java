package com.heartcar.brand.repositories;

import com.heartcar.brand.models.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by defri on 8/31/2017.
 */
@Repository
public interface BrandRepository  extends JpaRepository<Brand,Integer>{

    @Query("SELECT b from Brand b where b.office.isActive = :bool AND b.office.officeComplex.isActive = :bool")
    List<Brand> getBrandsByActiveOfficesAndCompelxes(@Param("bool") boolean bool);

    @Query("SELECT b from Brand b where b.office.isActive=true AND b.office.officeComplex.isActive = true AND b.office.officeComplex.id = :id")
    List<Brand> getBrandsByLoggedUser(@Param("id") int companyId);

    @Query("SELECT b from Brand b where b.active=true AND b.office.isActive=true AND b.office.officeComplex.isActive = true AND b.office.officeComplex.id = :id")
    List<Brand> getActiveBrandsByLoggedUser(@Param("id") int companyId);

    @Query("SELECT b from Brand b where b.name= :name and b.office.id = :officeId")
    Brand findBrandByName(@Param("name") String name,@Param("officeId") int officeId);

}
