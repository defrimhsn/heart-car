package com.heartcar.brand.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.heartcar.office.models.Office;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 8/31/2017.
 */
@Entity
@Table(name="brand")
public class Brand implements Serializable {

    @Id
    @Column(name="id")
    private int id;
    @NotNull
    @Column(name="name")
    private String name;
    @Column(name="description")
    private String description;
    @ManyToOne
    @JoinColumn(name="office_id")
    @JsonBackReference("brand")
    private Office office;
    @NotNull
    @Column(name="created_date")
    private Date createdDate;

    @Column(name="updated_date")
    private Date updatedDate;
    @NotNull
    @Column(name="is_active")
    private boolean active;

    @OneToMany(targetEntity = Category.class,mappedBy = "brand")
    @JsonManagedReference(value="category")
    private List<Category> categories;

    public Brand(int id, String name, String description, Office officeId, Date createdDate, Date updatedDate, boolean active) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.office = officeId;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.active = active;
    }


    public Brand() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office officeId) {
        this.office = officeId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Brand brand = (Brand) o;

        return name != null ? name.equals(brand.name) : brand.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return name +" [ "+office.getOfficeId()+" ]";
    }
}
