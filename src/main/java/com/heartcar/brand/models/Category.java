package com.heartcar.brand.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.car.models.Car;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 8/31/2017.
 */
@Entity
@Table(name="category")
public class Category implements Serializable {

    @Id
    @Column(name="id")
    private int id;
    @NotNull
    @Column(name="name")
    private String name;
    @Column(name="description")
    private String description;
    @NotNull
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="updated_date")
    private Date updatedDate;
    @NotNull
    @Column(name="is_active")
    private boolean active;
    @ManyToOne
    @JoinColumn(name="brand_id")
    @JsonBackReference("category")
    private Brand brand;
    @OneToMany(targetEntity = Car.class,mappedBy = "category")
    @JsonManagedReference("car")
    private List<Car> cars = new ArrayList<>();


    public Category(int id, String name, String description, Date createdDate, Date updatedDate, boolean active, Brand brand) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.active = active;
        this.brand = brand;
    }

    public Category() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return name != null ? name.equals(category.name) : category.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return getBrand().getName()+" ("+name+")"+" - [ "+getBrand().getOffice().getOfficeId()+" ] ";
    }
}
