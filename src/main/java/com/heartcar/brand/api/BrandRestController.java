package com.heartcar.brand.api;

import com.heartcar.brand.models.Brand;
import com.heartcar.brand.services.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by defri on 8/31/2017.
 */
@RestController
@RequestMapping(value="/api/brand")
public class BrandRestController {

    @Autowired
    private BrandService brandService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Brand> getAll(){
        return brandService.getAll();
    }

    @RequestMapping(value = "/public",method = RequestMethod.GET)
    public List<Brand> getBrandsByActiveOfficesAndCompelxes(){
        return brandService.getBrandsByActiveOfficesAndCompelxes(true);
    }


}
