package com.heartcar.brand.api;

import com.heartcar.brand.models.Category;
import com.heartcar.brand.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by defri on 9/4/2017.
 */
@RestController
@RequestMapping(value = "/api/category")
public class CategoryRestController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/public",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> findCategoriesByActiveParents(){
        return categoryService.findCategoriesByActiveParents(true);
    }
}
