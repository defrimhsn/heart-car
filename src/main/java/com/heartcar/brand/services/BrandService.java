package com.heartcar.brand.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.brand.models.Brand;
import com.heartcar.brand.repositories.BrandRepository;
import com.heartcar.office.models.Office;
import com.heartcar.office.repositories.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 8/31/2017.
 */
@Service
public class BrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private UserService userService;


    public List<Brand> getAll(){
        return brandRepository.findAll();
    }

    public List<Brand> getBrandsByActiveOfficesAndCompelxes(boolean bool){
        return brandRepository.getBrandsByActiveOfficesAndCompelxes(bool);
    }

    public List<Brand> getBrandsByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return brandRepository.getBrandsByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public List<Brand> getActiveBrandsByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return brandRepository.getActiveBrandsByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public HashMap<String,String> saveBrand(Map<String,String> map, int office) {
        Office requestOffice = new Office();
        HashMap<String,String> messageResponse = new HashMap<>();
        requestOffice = officeRepository.findOne(office);
        if(requestOffice==null){
            messageResponse.put("err_msg","Submission failed, office not found");
        } else{
            if(map.get("name")==null ||map.get("name").trim().isEmpty()){
                messageResponse.put("err_msg","Submission failed, brand name is empty");
            } else{
                if(brandRepository.findBrandByName(map.get("name"),office)!=null){
                      messageResponse.put("err_msg","Submission failed, brand exist already");
                } else {
                    Brand brand = new Brand();
                    brand.setOffice(requestOffice);
                    brand.setName(map.get("name"));
                    brand.setDescription(map.get("description"));
                    brand.setCreatedDate(new Date());
                    brand.setActive(true);
                    Brand createdBrand = brandRepository.save(brand);
                    if(createdBrand==null){
                        messageResponse.put("err_msg","Submission failed, something is wrong");
                    } else{
                        messageResponse.put("success_msg","Submission made successfully");
                    }
                }

            }
        }
        return messageResponse;
    }

    public HashMap<String,String> disable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Brand fromDB = brandRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        Brand updated = brandRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Brand fromDB = brandRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        Brand updated = brandRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;

    }

    public HashMap<String,String> updateBrand(Map<String,String> map) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Brand fromDB = brandRepository.findOne(Integer.parseInt(map.get("id")));
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, brand doesent exist");
        } else{
            fromDB.setDescription(map.get("description"));
            fromDB.setUpdatedDate(new Date());
            Brand updatedBrand = brandRepository.save(fromDB);
            if(updatedBrand==null){
                messageResponse.put("err_msg","Update failed, something went wrong");
            } else{
                messageResponse.put("success_msg","Update made successfully!");
            }
        }
        return messageResponse;
    }
}
