package com.heartcar.brand.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.repositories.UserRepository;
import com.heartcar.administration.services.UserService;
import com.heartcar.brand.models.Brand;
import com.heartcar.brand.models.Category;
import com.heartcar.brand.repositories.BrandRepository;
import com.heartcar.brand.repositories.CategoryRepository;
import com.heartcar.office.models.Office;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 8/31/2017.
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private UserService userService;

    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    public List<Category> findCategoriesByActiveParents(boolean bool){
        return categoryRepository.findCategoriesByActiveParents(bool);
    }

    public List<Category> getCategoriesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return categoryRepository.findCategoriesByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public List<Category> getActiveCategoriesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return categoryRepository.findActiveCategoriesByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }


    public HashMap<String,String> disable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Category fromDB = categoryRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(false);
        Category updated = categoryRepository.save(fromDB);
        if(!updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;
    }

    public HashMap<String,String> enable(int id) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Category fromDB = categoryRepository.findOne(id);
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, complex not found");
        }
        fromDB.setUpdatedDate(new Date());
        fromDB.setActive(true);
        Category updated = categoryRepository.save(fromDB);
        if(updated.isActive()){
            messageResponse.put("success_msg","Update made sucessfully");
        } else{
            messageResponse.put("err_msg","Update failed, something went wrong");
        }
        return messageResponse;

    }

    public HashMap<String,String> updateBrand(Map<String,String> map) {
        HashMap<String,String> messageResponse = new HashMap<>();
        Category fromDB = categoryRepository.findOne(Integer.parseInt(map.get("id")));
        if(fromDB==null){
            messageResponse.put("err_msg","Update failed, brand doesent exist");
        } else{
            fromDB.setDescription(map.get("description"));
            fromDB.setUpdatedDate(new Date());
            Category updatedBrand = categoryRepository.save(fromDB);
            if(updatedBrand==null){
                messageResponse.put("err_msg","Update failed, something went wrong");
            } else{
                messageResponse.put("success_msg","Update made successfully!");
            }
        }
        return messageResponse;
    }

    public HashMap<String,String> saveCategory(Map<String,String> map, int brand) {
        Brand requestBrand = new Brand();
        HashMap<String,String> messageResponse = new HashMap<>();
        requestBrand = brandRepository.findOne(brand);
        if(requestBrand==null){
            messageResponse.put("err_msg","Submission failed, brand not found");
        } else{
            if(map.get("name")==null ||map.get("name").trim().isEmpty()){
                messageResponse.put("err_msg","Submission failed, category name is empty");
            } else{
                if(categoryRepository.findCategoryByName(map.get("name"),brand)!=null){
                    messageResponse.put("err_msg","Submission failed, category exist already");
                } else {
                    Category category = new Category();
                    category.setBrand(requestBrand);
                    category.setName(map.get("name"));
                    category.setDescription(map.get("description"));
                    category.setCreatedDate(new Date());
                    category.setActive(true);
                    Category createdCategory = categoryRepository.save(category);
                    if(createdCategory==null){
                        messageResponse.put("err_msg","Submission failed, something is wrong");
                    } else{
                        messageResponse.put("success_msg","Submission made successfully");
                    }
                }

            }
        }
        return messageResponse;

    }
}
