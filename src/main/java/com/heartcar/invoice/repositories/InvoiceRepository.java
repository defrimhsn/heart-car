package com.heartcar.invoice.repositories;

import com.heartcar.invoice.models.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by defri on 9/14/2017.
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice,Integer> {
    Invoice findFirstByOrderByIdDesc();

    @Query("SELECT i from Invoice i where i.car.isActive = true " +
            "and i.car.category.active = true " +
            "and i.car.category.brand.active = true " +
            "and i.car.category.brand.office.isActive = true " +
            "and i.car.category.brand.office.officeComplex.isActive = true and i.car.category.brand.office.officeComplex.id = :id")
    List<Invoice> findInvoicesByLoggedUser(@Param("id") int companyId);

    @Query("SELECT i from Invoice i where i.car.isActive = true " +
            "and i.car.category.active = true " +
            "and i.car.category.brand.active = true " +
            "and i.car.category.brand.office.isActive = true " +
            "and i.car.category.brand.office.officeComplex.isActive = true and i.car.category.brand.office.officeComplex.id = :id order by i.id DESC ")
    List<Invoice> getFiveLastInvoices(@Param(("id")) int companyId);
}
