package com.heartcar.invoice.services;

import com.heartcar.administration.models.User;
import com.heartcar.administration.services.UserService;
import com.heartcar.car.models.Car;
import com.heartcar.car.repositories.CarRepository;
import com.heartcar.invoice.models.Invoice;
import com.heartcar.invoice.repositories.InvoiceRepository;
import com.heartcar.tenant.models.Tenant;
import com.heartcar.tenant.repositories.TenantRepository;
import com.heartcar.tenant.services.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 9/14/2017.
 */
@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private UserService userService;

    public List<Invoice> getAll() {
        return invoiceRepository.findAll();
    }


    public HashMap<String, String> saveInvoice(Map<String, String> map) {
        HashMap<String,String> messageResponse = new HashMap<>();
        boolean allNotNull = false;
        for(Map.Entry<String,String> entry : map.entrySet()){
            if(entry.getKey()!=null && entry.getValue()!=null){
                allNotNull = true;
            } else{
                allNotNull = false;
            }
        }
        if (allNotNull){
            Car requestCar = carRepository.findOne(Integer.parseInt(map.get("car")));
            Tenant requestTenant = tenantRepository.findOne(Integer.parseInt(map.get("tenant")));
            double price = Double.parseDouble(map.get("price"));
            SimpleDateFormat fromRequest = new SimpleDateFormat("dd/MM/yyyy");
            Date addmissionDate = null;
            try {
                addmissionDate = fromRequest.parse(map.get("adDate"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Invoice invoice = new Invoice();
            invoice.setCar(requestCar);
            invoice.setTenant(requestTenant);
            invoice.setPrice(price);
            invoice.setCreatedDate(new Date());
            invoice.setSubmissionDate(new Date());
            invoice.setAdmissionDate(addmissionDate);
            invoice.setStatus(true);
            invoice.setActive(true);
            Invoice addedInvoice = invoiceRepository.save(invoice);
            if(addedInvoice!=null){
                messageResponse.put("success_msg","Submission made successfully!, click Print to print this invoice");
                Invoice responseInvoice = invoiceRepository.findFirstByOrderByIdDesc();
                messageResponse.put("inv_id",responseInvoice.getId()+"");
            } else{
                messageResponse.put("err_msg","Submission failed, something went wrong");
            }
        } else{
            messageResponse.put("err_msg","Submission failed, something went wrong during the request");
        }
        return messageResponse;
    }


    public Invoice getOne(int id) {
        return invoiceRepository.findOne(id);
    }

    public List<Invoice> getInvoicesByLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User loggedUser = userService.findUserByEmployersUsername(authentication.getName());
        return invoiceRepository.findInvoicesByLoggedUser(loggedUser.getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }

    public Invoice getLast(){
        return invoiceRepository.findFirstByOrderByIdDesc();
    }

    public List<Invoice> getFiveLastInvoices() {
        return invoiceRepository.getFiveLastInvoices(userService.getLoggedUser().getEmployer().getPosition().getOffice().getOfficeComplex().getId());
    }
}