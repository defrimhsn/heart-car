package com.heartcar.invoice.controllers;

import com.heartcar.administration.services.UserService;
import com.heartcar.car.services.CarService;
import com.heartcar.invoice.services.InvoiceService;
import com.heartcar.tenant.services.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by defri on 9/14/2017.
 */
@Controller
@RequestMapping(value = "/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private CarService carService;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String initInvoiceView(Model model){
        model.addAttribute("invoices",invoiceService.getInvoicesByLoggedUser());
        model.addAttribute("cars",carService.findActiveCarsbyLoggedUser());
        model.addAttribute("tenants",tenantService.getAll());
        model.addAttribute("user",userService.getLoggedUser());
        return "invoice/invoice";
    }

    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HashMap<String,String> saveInvoice(@RequestBody Map<String,String> map){
            return invoiceService.saveInvoice(map);
    }

    @RequestMapping(value = "/print/{id}",method = RequestMethod.GET)
    public String printInvoice(@PathVariable int id, Model model){
        model.addAttribute("invoice",invoiceService.getOne(id));
        return "invoice/invoice_print_version";
    }
}
