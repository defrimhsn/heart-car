package com.heartcar.invoice.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.heartcar.car.models.Car;
import com.heartcar.tenant.models.Tenant;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by defri on 9/14/2017.
 */
@Entity
@Table(name="invoice")
public class Invoice implements Serializable{

    @Id
    @Column(name="id")
    private int id;
    @ManyToOne
    @JoinColumn(name="car_id")
    @JsonBackReference("invoice")
    private Car car;
    @ManyToOne
    @JoinColumn(name="tenant_id")
    @JsonBackReference("tenant")
    private Tenant tenant;
    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "submission_date")
    private Date submissionDate;
    @Column(name="admission_date")
    private Date admissionDate;
    @Column(name = "updated_date")
    private Date updatedDate;
    @Column(name = "price")
    private double price;
    @Column(name = "status")
    private boolean status;
    @Column(name = "is_active")
    private boolean active;

    public Invoice(int id, Car car, Tenant tenant, Date createdDate, Date submissionDate, Date admissionDate, Date updatedDate, double price, boolean status, boolean active) {
        this.id = id;
        this.car = car;
        this.tenant = tenant;
        this.createdDate = createdDate;
        this.submissionDate = submissionDate;
        this.admissionDate = admissionDate;
        this.updatedDate = updatedDate;
        this.price = price;
        this.status = status;
        this.active = active;
    }

    public Invoice() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", car=" + car +
                ", tenant=" + tenant +
                ", createdDate=" + createdDate +
                ", submissionDate=" + submissionDate +
                ", admissionDate=" + admissionDate +
                ", updatedDate=" + updatedDate +
                ", price=" + price +
                ", status=" + status +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Invoice invoice = (Invoice) o;

        return getId() == invoice.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }
}
