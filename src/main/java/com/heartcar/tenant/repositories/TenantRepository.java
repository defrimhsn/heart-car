package com.heartcar.tenant.repositories;

import com.heartcar.tenant.models.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by defri on 9/14/2017.
 */
@Repository
public interface TenantRepository extends JpaRepository<Tenant,Integer>{
}
