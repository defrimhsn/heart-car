package com.heartcar.tenant.services;

import com.heartcar.tenant.models.Tenant;
import com.heartcar.tenant.repositories.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by defri on 9/14/2017.
 */
@Service
public class TenantService {

    @Autowired
    private TenantRepository tenantRepository;

    public List<Tenant> getAll(){
        return tenantRepository.findAll();
    }

    public Tenant saveTenant(String personalNumber,String name,String surname,String city,String country){
        if(personalNumber==null || name==null || surname==null || city==null || country==null){
            return null;
        } else{
            Tenant tenant = new Tenant();
            tenant.setPersonalNumber(personalNumber);
            tenant.setName(name);
            tenant.setSurname(surname);
            tenant.setCreatedDate(new Date());
            tenant.setCity(city);
            tenant.setCountry(country);

            return tenantRepository.saveAndFlush(tenant);
        }

    }

    public HashMap<String, String> saveNewTenant(Map<String, String> map) {
        HashMap<String,String> messageResponse = new HashMap<>();
        if(map.isEmpty()){
            messageResponse.put("err_msg","Submission failed, something went wrong during the request");
        } else{
            String personalNumber = map.get("personalNumber");
            String name = map.get("name");
            String surname = map.get("surname");
            String city = map.get("city");
            String country = map.get("country");
            Tenant addedTenant = saveTenant(personalNumber,name,surname,city,country);
            if(addedTenant!=null){
                messageResponse.put("success_msg","Submission made successfully");
                messageResponse.put("updated_list",tenantRepository.findAll().toString());
            } else{
                messageResponse.put("err_msg","Submission failed, something went wrong!");
            }
        }
        return messageResponse;
    }
}
