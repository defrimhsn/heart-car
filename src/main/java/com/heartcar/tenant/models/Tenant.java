package com.heartcar.tenant.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.heartcar.invoice.models.Invoice;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by defri on 9/14/2017.
 */
@Entity
@Table(name = "tenant")
public class Tenant implements Serializable{

    @Id
    @Column(name="id")
    private int id;
    @Column(name="id_number")
    private String personalNumber;
    @Column(name="name")
    private String name;
    @Column(name="surname")
    private String surname;
    @Column(name="city")
    private String city;
    @Column(name="country")
    private String country;
    @Column(name="created_date")
    private Date createdDate;

    @OneToMany(targetEntity = Invoice.class,mappedBy = "tenant")
    @JsonManagedReference("tenant")
    private List<Invoice> invoices = new ArrayList<>();

    public Tenant() {
    }

    public Tenant(int id, String personalNumber, String name, String surname, String city, String country, Date createdDate, List<Invoice> invoices) {
        this.id = id;
        this.personalNumber = personalNumber;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.country = country;
        this.createdDate = createdDate;
        this.invoices = invoices;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tenant tenant = (Tenant) o;

        return getPersonalNumber() != null ? getPersonalNumber().equals(tenant.getPersonalNumber()) : tenant.getPersonalNumber() == null;
    }

    @Override
    public int hashCode() {
        return getPersonalNumber() != null ? getPersonalNumber().hashCode() : 0;
    }

    @Override
    public String toString() {
        return name+" : "+ surname+" ["+personalNumber+"]";
    }
}
